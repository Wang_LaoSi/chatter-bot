# -*- coding: utf-8 -*-
from chatterbot import ChatBot
import logging
"""反馈式的聊天机器人，会根据你的反馈进行学习"""
# 把下面这行前的注释去掉，可以把一些信息写入日志中
# logging.basicConfig(level=logging.INFO)
# 创建一个聊天机器人
bot = ChatBot(
   'Feedback Learning Bot',
   storage_adapter='chatterbot.storage.JsonFileStorageAdapter',
   logic_adapters=[
       'chatterbot.logic.BestMatch'
   ],
   input_adapter='chatterbot.input.TerminalAdapter',
 output_adapter='chatterbot.output.TerminalAdapter')
DEFAULT_SESSION_ID = bot.default_session.id
def get_feedback():
   from chatterbot.utils import input_function
   text = input_function()
   if 'Yes' in text:
       return True
   elif 'No' in text:
       return False
   else:
       print('Please type either "Yes" or "No"')
       return get_feedback()
print('Type something to begin...')
# 每次用户有输入内容，这个循环就会开始执行
while True:
   try:
       input_statement = bot.input.process_input_statement()
       statement, response = bot.generate_response(input_statement, DEFAULT_SESSION_ID)
       print('\n Is "{}" this a coherent response to "{}"? \n'.format(response, input_statement))
       if get_feedback():
           bot.learn_response(response,input_statement)
       bot.output.process_response(response)
       # 更新chatbot的历史聊天数据
       bot.conversation_sessions.update(
           bot.default_session.id_string,
           (statement, response, )
       )
   # 直到按ctrl-c 或者 ctrl-d 才会退出
   except (KeyboardInterrupt, EOFError, SystemExit):
       break
#反馈式学习聊天机器人

#使用Ubuntu数据集构建聊天机器人

from chatterbot import ChatBot
import logging
'''''这是一个使用Ubuntu语料构建聊天机器人的例子'''
# 允许打日志logging.basicConfig(level=logging.INFO)
chatbot = ChatBot(
   'Example Bot',   trainer='chatterbot.trainers.UbuntuCorpusTrainer')
# 使用Ubuntu数据集开始训练
chatbot.train()
# 我们来看看训练后的机器人的应答
response = chatbot.get_response('How are you doing today?')
print(response)