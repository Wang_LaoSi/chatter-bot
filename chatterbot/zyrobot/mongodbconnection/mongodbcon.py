from pymongo import MongoClient
from config import mongo_user, mongo_pwd, mongo_url, mongo_port


client = MongoClient('mongodb://'+str(mongo_user)+':'+str(mongo_pwd)+'@'+str(mongo_url)+':'+str(mongo_port))  # 测试库


def dropTable(table):
    try:
        '''删除表'''
        client.drop_database(table)
    except:
        pass
    finally:
        pass


# 查询是否存在表
def seltable(tablename):
    global client
    try:
        db = client[str(tablename)]
        return db.statements.count()
    except Exception as e:
        print(e)
    finally:
        pass


# 删除元素
def delonedata(cmpid, HoodleMobile, attitude, deltext, dbname):
    global client
    try:
        db = client[str(str(dbname)+'database'+str(cmpid))]

        db['database'+str(cmpid)+'-HoodleMobile'+str(HoodleMobile)].update(
            {'text': attitude},
            {'$pull': {"in_response_to": {"text": deltext}}},
        )

        db['database'+str(cmpid)+'-HoodleMobile'+str(HoodleMobile)].remove({'text': str(deltext)})  # 删除yy=5的记录

    except:

        pass

    finally:
        pass


# 删除公共类
def delcommon(answertypeid, answerdatanum):
    global client
    try:
        db = client['answertype' + str(answertypeid)]
        db.statements.update(
            {'text': answertypeid},
            {'$pull': {"in_response_to": {"text": answerdatanum}}},
        )

        db.statements.remove({'text': str(answerdatanum)})  # 删除yy=5的记录

    except:
        pass
    finally:
        pass


# 删除数据包含编码
def deldata_id(cmpid, deldata, newdatabasename, dbname):

    global client
    try:
        db = client[str(str(dbname)+'database' + str(cmpid))]
        if newdatabasename != '':
            name = str('database' + str(cmpid)) + '-' + newdatabasename
        else:
            name = 'statements'

        deltabledata = db[name]
        deltabledata.remove(
            {'in_response_to.text': str(deldata)}

        )
        return db[name].remove({'text': str(deldata)})['n']

    except Exception as e:
        print('err'+str(e))
        pass
    finally:
        pass


# 删除数据
def deldata(cmpid, deldata, newdatabasename, dbname):

    global client
    try:
        db = client[str(str(dbname)+'database' + str(cmpid))]
        if newdatabasename != '':
            name = str('database' + str(cmpid)) + '-' + newdatabasename
        else:
            name = 'statements'
        return db[name].remove({'text': str(deldata)})['n']

    except:
        pass
    finally:
        pass


# 删除公共问题库数据
def delcommondata(industryid, industryquestion):
    global client
    try:

        db = client[str('industry' + str(industryid))]

        return db.statements.remove({'text': str(industryquestion)})['n']
    except:
        pass
    finally:
        pass


# 删除元素
def delindustryquestion(industryid, industryquestion, industryanswerid):
    global client
    try:
        db = client["industry"+str(industryid)]
        db.statements.update(
            {'text': {'$ne': industryanswerid}},
            {'$pull': {"in_response_to": {"text": industryquestion}}},
        )

        db.statements.remove({'text': str(industryquestion)})

    except:
        pass
    finally:
        pass


def dropservice(table):
    global client
    try:
        '''删除表'''

        client.drop_database(table)
    except:
        pass
    finally:
        pass


def dropset(cmpid, variate, dbname):
    global client
    try:
        '''删除表'''
        db = client[str(dbname)+'database' + str(cmpid)]
        db[variate].drop()
    except:
        pass
    finally:
        pass


def drophoodleMobile(table):
    global client
    try:
        '''删除表'''
        client.drop_database(table)
    except:
        pass
    finally:
        pass


def selalltable():
    global client
    try:
        alltable = client.database_names()
        return alltable
    except:
        pass
    finally:
        pass


# 判断表是否存在
def ifexist(database):
    global client
    try:
        dblist = client.database_names()
        if str(database) in dblist:
            return '1'
        else:
            return '0'
    except:
        pass
    finally:
        pass
