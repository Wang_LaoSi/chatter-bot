#encoding=UTF-8

import warnings
from chatterbot.zyrobot.decorators import time_statistic
warnings.filterwarnings("ignore")
from ...chatterbot  import ChatBot
from ...trainers import ListTrainer
from pymongo import MongoClient
from config import mongo_user, mongo_pwd,mongo_url,mongo_port

database_uri=MongoClient('mongodb://'+str(mongo_user)+':'+str(mongo_pwd)+'@'+str(mongo_url)+':'+str(mongo_port)) #测试库

#查询引导
def getguidance(cmpid,textnew,dbname):
    bot = ChatBot(
        str('guidance'+ str(cmpid)),
        storage_adapter='chatterbot.storage.MongoDatabaseAdapter',
        logic_adapters=[
           {
               'import_path': 'chatterbot.logic.BestMatch',
                "statement_comparison_function": "chatterbot.comparisons.levenshtein_distance",
                "response_selection_method": "chatterbot.response_selection.get_random_response"
           },
           {

               'import_path': 'chatterbot.logic.LowConfidenceAdapter',
               'threshold': 0.65,
               'default_response':''
           }
        ],
        database=str(dbname)+'databaseguidance'+ str(cmpid),
        database_uri=database_uri,
        read_only=True
    )
    ans = bot.get_response(textnew)
    return  str(ans)


#添加引导数据
def addguidance(cmpid,guidance,guidanceid,dbname):
    bot = ChatBot(
        str('guidance'+ str(cmpid)),
        storage_adapter='chatterbot.storage.MongoDatabaseAdapter',
        database_uri=database_uri,
        database=str(dbname)+'databaseguidance'+ str(cmpid)
    )
    bot.set_trainer(ListTrainer)
    bot.train([guidance,guidanceid])

def getanw(cmpid,textnew,cid,serviceid,dbname):
        bot = ChatBot(
            str(cmpid),
            storage_adapter='chatterbot.storage.MongoDatabaseAdapter',
            logic_adapters=[
               {
                   'import_path': 'chatterbot.logic.BestMatch',
                    "statement_comparison_function": "chatterbot.comparisons.levenshtein_distance",
                    "response_selection_method": "chatterbot.response_selection.get_random_response"
               },
               {

                   'import_path': 'chatterbot.logic.LowConfidenceAdapter',
                   'threshold': 0.3,
                   'default_response':'不好意思吖，我还在学习中，目前不知道怎么回答这个问题呢(O_O)?' ,
               }
            ],
            filters=[  #过滤器消除了可能重复的响应，防止聊天机器人重复它最近说过的话。
                'chatterbot.filters.RepetitiveResponseFilter'
            ],
            database=str(dbname)+'database'+ str(cmpid),
            database_uri=database_uri,
            read_only=True
        )
        ans = bot.get_response(textnew)
        #print('ddddffffeeee',ans)
        return  str(ans)

#(已不使用)
def getcloseresultjudge(cmpid,textnew,cid,serviceid):
        bot = ChatBot(
            str(cmpid),
            storage_adapter='chatterbot.storage.MongoDatabaseAdapter',
            logic_adapters=[
               {
                   'import_path': 'chatterbot.logic.BestMatch',
                    "statement_comparison_function": "chatterbot.comparisons.levenshtein_distance",
                    "response_selection_method": "chatterbot.response_selection.get_random_response"
               },
               {

                   'import_path': 'chatterbot.logic.LowConfidenceAdapter',
                   'threshold': 0.8,
                   'default_response':'不好意思吖，我还在学习中，目前不知道怎么回答这个问题呢(O_O)?' ,
               }
            ],
            filters=[  #过滤器消除了可能重复的响应，防止聊天机器人重复它最近说过的话。
                'chatterbot.filters.RepetitiveResponseFilter'
            ],
            database='closeresult',
            database_uri=database_uri,
            read_only=True
        )
        ans = bot.get_response(textnew)
        return  str(ans)


#打断忽略词匹配
def getlosejudge(cmpid,textnew,cid,serviceid):
        bot = ChatBot(
            str(cmpid),
            storage_adapter='chatterbot.storage.MongoDatabaseAdapter',
            logic_adapters=[
               {
                   'import_path': 'chatterbot.logic.BestMatch',
                    "statement_comparison_function": "chatterbot.comparisons.levenshtein_distance",
                    "response_selection_method": "chatterbot.response_selection.get_random_response"
               },
               {

                   'import_path': 'chatterbot.logic.LowConfidenceAdapter',
                   'threshold': 0.5,
                   'default_response':'' ,
               }
            ],
            filters=[  #过滤器消除了可能重复的响应，防止聊天机器人重复它最近说过的话。
                'chatterbot.filters.RepetitiveResponseFilter'
            ],
            database='answertype13',
            database_uri=database_uri,
            read_only=True
        )
        ans = bot.get_response(textnew)
        return  str(ans)

@time_statistic(2,'cid')
def getanwservice(cmpid,textnew,cid,serviceid,askid,operation,industryid,session_data,session_sort,dbname):
        #print(cmpid,serviceid,dbname)
        bot = ChatBot(
            str(cmpid),
            storage_adapter='chatterbot.storage.MongoDatabaseAdapter',
            logic_adapters=[
               {
                   'import_path': 'chatterbot.logic.BestMatchsession',
                   "statement_comparison_function": "chatterbot.comparisons.levenshtein_distance",
                   "response_selection_method": "chatterbot.response_selection.get_random_response",
                   #'import_path': 'chatterbot.logic.LowConfidenceAdapter',
                   'threshold': 0.85,
                   'default_response':'不好意思吖，我还在学习中，目前不知道怎么回答这个问题呢(O_O)?' ,
                   'cid':str(cid),
                   'cmpid':str(cmpid),
                   'serviceid':str(serviceid),
                   'askid':str(askid),
                   'operation':str(operation),
                   'industryid':str(industryid),
                   'session_data':str(session_data),
                   'session_sort':str(session_sort),
                   'dbname': str(dbname),

               }
            ],
            filters=[  #过滤器消除了可能重复的响应，防止聊天机器人重复它最近说过的话。
                'chatterbot.filters.RepetitiveResponseFilter'
            ],
            database=str(dbname)+'database'+ str(cmpid),
            newdatabasename='database' + str(cmpid) + '-service' + str(serviceid),
            database_uri=database_uri,
            read_only=True
        )
        ans = bot.get_response(textnew)
        #print("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx")
        #print(ans)
        #print("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx")
        return  str(ans)

#批量匹配问题库
def getanwservicequestion(cmpid, textnew, cid, serviceid, askid, operation, industryid, session_data, session_sort,dbname):
    bot = ChatBot(
        str(cmpid),
        storage_adapter='chatterbot.storage.MongoDatabaseAdapter',
        logic_adapters=[
            {
                'import_path': 'chatterbot.logic.BestMatchquestion',
                "statement_comparison_function": "chatterbot.comparisons.levenshtein_distance",
                "response_selection_method": "chatterbot.response_selection.get_random_response",
                # 'import_path': 'chatterbot.logic.LowConfidenceAdapter',
                'threshold': 0.85,
                'default_response': '不好意思吖，我还在学习中，目前不知道怎么回答这个问题呢(O_O)?',
                'cid': str(cid),
                'cmpid': str(cmpid),
                'serviceid': str(serviceid),
                'askid': str(askid),
                'operation': str(operation),
                'industryid': str(industryid),
                'session_data': str(session_data),
                'session_sort': str(session_sort),
                'dbname': str(dbname),

            }
        ],
        filters=[  # 过滤器消除了可能重复的响应，防止聊天机器人重复它最近说过的话。
            'chatterbot.filters.RepetitiveResponseFilter'
        ],
        database=str(dbname)+'database' + str(cmpid) ,
        newdatabasename='database' + str(cmpid) + '-service' + str(serviceid),
        database_uri=database_uri,
        read_only=True
    )
    ans = bot.get_response(textnew)
    return str(ans)

#匹配在线质检问题库
def getanwqualityquestion(cmpid, textnew, cid, serviceid, askid, operation, industryid, session_data, session_sort,dbname):
    bot = ChatBot(
        str(cmpid),
        storage_adapter='chatterbot.storage.MongoDatabaseAdapter',
        logic_adapters=[
            {
                'import_path': 'chatterbot.logic.Bestqualitquestion',
                "statement_comparison_function": "chatterbot.comparisons.levenshtein_distance",
                "response_selection_method": "chatterbot.response_selection.get_random_response",
                # 'import_path': 'chatterbot.logic.LowConfidenceAdapter',
                'threshold': 0.85,
                'default_response': '',
                'cid': str(cid),
                'cmpid': str(cmpid),
                'serviceid': str(serviceid),
                'askid': str(askid),
                'operation': str(operation),
                'industryid': str(industryid),
                'session_data': str(session_data),
                'session_sort': str(session_sort),
                'dbname': str(dbname),

            }
        ],
        filters=[  # 过滤器消除了可能重复的响应，防止聊天机器人重复它最近说过的话。
            'chatterbot.filters.RepetitiveResponseFilter'
        ],
        database=str(dbname)+'database' + str(cmpid) ,
        newdatabasename='database' + str(cmpid),
        database_uri=database_uri,
        read_only=True
    )
    ans = bot.get_response(textnew)
    return str(ans)


def getanswertypecommon(cmpid,textnew,type):
        print('d')
        bot = ChatBot(
            str(cmpid),
            storage_adapter='chatterbot.storage.MongoDatabaseAdapter',
            logic_adapters=[
               {
                   'import_path': 'chatterbot.logic.BestMatchsession',
                   "statement_comparison_function": "chatterbot.comparisons.levenshtein_distance",
                   "response_selection_method": "chatterbot.response_selection.get_random_response",
                   #'import_path': 'chatterbot.logic.LowConfidenceAdapter',
                   'threshold': 0.6,
                   #'default_response':'不好意思吖，我还在学习中，目前不知道怎么回答这个问题呢(O_O)?'
                   'default_response':''
               }
            ],
            filters=[  #过滤器消除了可能重复的响应，防止聊天机器人重复它最近说过的话。
                'chatterbot.filters.RepetitiveResponseFilter'
            ],
            database='answertype'+ str(type),
            database_uri=database_uri,
            read_only=True
        )
        ans = bot.get_response(textnew)
        return  str(ans)

#查询唤醒词
def getawait(cmpid,textnew,type):
        bot = ChatBot(
            str(cmpid),
            storage_adapter='chatterbot.storage.MongoDatabaseAdapter',
            logic_adapters=[
               {
                   'import_path': 'chatterbot.logic.BestMatchsession',
                   "statement_comparison_function": "chatterbot.comparisons.levenshtein_distance",
                   "response_selection_method": "chatterbot.response_selection.get_random_response",
                   #'import_path': 'chatterbot.logic.LowConfidenceAdapter',
                   'threshold': 0.5,
                   #'default_response':'不好意思吖，我还在学习中，目前不知道怎么回答这个问题呢(O_O)?'
                   'default_response':''
               }
            ],
            filters=[  #过滤器消除了可能重复的响应，防止聊天机器人重复它最近说过的话。
                'chatterbot.filters.RepetitiveResponseFilter'
            ],
            database='answertype'+ str(type),
            database_uri=database_uri,
            read_only=True
        )
        ans = bot.get_response(textnew)
        return  str(ans)

def selanswertype(cmpid,textnew,cid,serviceid,answertypeid):
        bot = ChatBot(
            str(cmpid),
            storage_adapter='chatterbot.storage.MongoDatabaseAdapter',
            logic_adapters=[
               {
                   'import_path': 'chatterbot.logic.BestMatch',
                    "statement_comparison_function": "chatterbot.comparisons.levenshtein_distance",
                    "response_selection_method": "chatterbot.response_selection.get_random_response"
               },
               {

                   'import_path': 'chatterbot.logic.LowConfidenceAdapter',
                   'threshold': 0.3,
                   'default_response':'' ,
               }
            ],
            filters=[  #过滤器消除了可能重复的响应，防止聊天机器人重复它最近说过的话。
                'chatterbot.filters.RepetitiveResponseFilter'
            ],
            database='answertype'+ str(answertypeid),
            database_uri=database_uri,
            read_only=True
        )
        ans = bot.get_response(textnew)
        return  str(ans)



#添加问答
def addstatement(cmpid,question,answer,mindatabasename,dbname):
        if mindatabasename!="":
            mindatabasename = 'database'+ str(cmpid) +'-'+str(mindatabasename)
        else:
            mindatabasename = 'statements'
        #print(cmpid,mindatabasename)
        bot = ChatBot(
            str(cmpid),
            storage_adapter='chatterbot.storage.MongoDatabaseAdapter',
            database_uri=database_uri,
            database=str(dbname)+'database'+ str(cmpid),
            newdatabasename = mindatabasename
        )
        bot.set_trainer(ListTrainer)
        bot.train([question,answer])

#添加行业库
def addindustry(industryid,question,answer,mindatabasename):

        mindatabasename = 'statements'
        bot = ChatBot(
            str(industryid),
            storage_adapter='chatterbot.storage.MongoDatabaseAdapter',
            database_uri=database_uri,
            database='industry'+ str(industryid),
            newdatabasename = mindatabasename
        )
        bot.set_trainer(ListTrainer)
        bot.train([question,answer])

#添加答案类型
def addanswertype(answertypeid,question):
        bot = ChatBot(
            str(answertypeid),
            storage_adapter='chatterbot.storage.MongoDatabaseAdapter',
            database_uri=database_uri,
            database='answertype'+ str(answertypeid)
        )
        bot.set_trainer(ListTrainer)
        bot.train([question,answertypeid])

#添加拨打结果
def addcallresults(id,callresults):
        bot = ChatBot(
            "closeresult",
            storage_adapter='chatterbot.storage.MongoDatabaseAdapter',
            database_uri=database_uri,
            database='closeresult'
        )
        bot.set_trainer(ListTrainer)
        bot.train([callresults,id])

#调用寒暄语句
def getgreeting(cmpid,textnew):
        bot = ChatBot(
            'allgreeting',
            storage_adapter='chatterbot.storage.MongoDatabaseAdapter',
            logic_adapters=[
                 {
                   'import_path': 'chatterbot.logic.BestMatch',
                    "statement_comparison_function": "chatterbot.comparisons.levenshtein_distance",
                    "response_selection_method": "chatterbot.response_selection.get_random_response"
               },
               {

                   'import_path': 'chatterbot.logic.LowConfidenceAdapter',
                   'threshold': 0.2,
                   'default_response': '不好意思吖，我还在学习中，目前不知道怎么回答这个问题呢(O_O)?'
               }
            ],
            database='greeting1',
            database_uri=database_uri,
            read_only=True
        )
        ans = bot.get_response(textnew)
        return str(ans)

#添加表情
def addmood(mood,answerid):
        bot = ChatBot(
            'mood',
            storage_adapter='chatterbot.storage.MongoDatabaseAdapter',
            database_uri=database_uri,
            database='databasemood'
        )
        bot.set_trainer(ListTrainer)
        bot.train([mood,'m'+ str(answerid)])

#获取表情
def getmood(textnew):
        bot = ChatBot(
            'mood',
            storage_adapter='chatterbot.storage.MongoDatabaseAdapter',
            database='databasemood',
            database_uri=database_uri,
            read_only=True
        )

        ans = bot.get_response(textnew)
        return  str(ans)



#添加公司简介
def addcompany(question,answer):
        bot = ChatBot(
            'company',
            storage_adapter='chatterbot.storage.MongoDatabaseAdapter',
            database='databasecompany',
            database_uri=database_uri,
            read_only=True
        )
        bot.set_trainer(ListTrainer)
        bot.train([question,answer])



#查询公司简介
def selcompany(question):
        bot = ChatBot(
            'company',
            storage_adapter='chatterbot.storage.MongoDatabaseAdapter',
            logic_adapters=[
                     {
                       'import_path': 'chatterbot.logic.BestMatch',
                        "statement_comparison_function": "chatterbot.comparisons.levenshtein_distance",
                        "response_selection_method": "chatterbot.response_selection.get_first_response"
                   },
                   {

                       'import_path': 'chatterbot.logic.LowConfidenceAdapter',
                       'threshold': 0.60,
                       'default_response': '不好意思吖，我还在学习中，目前不知道怎么回答这个问题呢(O_O)?'
                   }
                ],
            database='databasecompany',
            database_uri=database_uri,
            read_only=True
        )
        ans = bot.get_response(question)
        return  str(ans)


#添加特殊答案
def addspecial(cmpid,HoodleMobile,spwcialwords,attitude,dbname):


        bot = ChatBot(
            'special',
            storage_adapter='chatterbot.storage.MongoDatabaseAdapter',
            database_uri=database_uri,
            read_only=True,
            database=str(dbname)+'database'+ str(cmpid),
            newdatabasename = 'database'+str(cmpid)+'-HoodleMobile'+str(HoodleMobile)
        )
        bot.set_trainer(ListTrainer)
        bot.train([spwcialwords,attitude])


#添加行业公共问题
def addtradequestioncommon(trade,question,answerid):
        bot = ChatBot(
            'industry',
            storage_adapter='chatterbot.storage.MongoDatabaseAdapter',
            database='industry'+str(trade),
            database_uri=database_uri,
            read_only=True
        )
        bot.set_trainer(ListTrainer)
        bot.train([question,answerid])
