import os
import pickle
import warnings
import numpy as np
from .FeaturesExtractor import FeaturesExtractor
import sys

warnings.filterwarnings("ignore")

# females_training_path = females_files_path
# males_training_path   = males_files_path
# error                 = 0
# total_sample          = 0
features_extractor = FeaturesExtractor()
# load models"females.gmm", "males.gmm"
females_gmm = pickle.load(open(sys.path[0] + '/chatterbot/zyrobot/projectsql/females.gmm', 'rb'))
males_gmm = pickle.load(open(sys.path[0] + '/chatterbot/zyrobot/projectsql/males.gmm', 'rb'))


# females_gmm = pickle.load(open('../females.gmm', 'rb'))
# males_gmm   = pickle.load(open('../males.gmm', 'rb'))

def identify_gender(vector):
    is_female_scores = np.array(females_gmm.score(vector))
    is_female_log_likelihood = is_female_scores.sum()

    is_male_scores = np.array(males_gmm.score(vector))
    is_male_log_likelihood = is_male_scores.sum()
    print(is_male_log_likelihood, is_female_log_likelihood)
    if is_male_log_likelihood > is_female_log_likelihood:
        winner = "males"
    else:
        winner = "females"
    return winner


def speed_process(file):
    try:
        vector = features_extractor.extract_features(file)
        winner = identify_gender(vector)
        print("%10s %3s %1s" % ("+ IDENTIFICATION", ":", winner))
        return winner
    except Exception as e:
        print(e)
        return '0'

# speed_process('D:\Pro\Chatterbot\ChatterBotmaster\chatterbot\zyrobot\projectsql\\15643922627520182.wav')
