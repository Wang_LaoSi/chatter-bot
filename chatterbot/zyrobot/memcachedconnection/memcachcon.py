# -*-coding:utf-8-*-
import datetime
import sys
import time

import memcache

from config import memcache_url, memcache_port
from logging import getLogger
web_logger = getLogger('web_logger')

mc = memcache.Client(["" + str(memcache_url) + ":" + str(memcache_port) + ""], debug=True)  # 测试库


# 查询是否存在某个词
def if_exist(key):
    try:
        ifexist = mc.get(key)
        # 不存在None
        return ifexist
    except Exception as e:
        web_logger.error('error:%s %s', type(e), e)


# 新加缓存对
def set_cach(key, value):
    try:
        if type(value) == str or type(value) == int:
            mc.set(key, str(value), time=1200)
        else:
            mc.set(key, value, time=1200)
    except Exception as e:
        web_logger.error('error:%s %s',  type(e), e)


def set_cach_not_time(key, value):
    try:
        if type(value) == str or type(value) == int:
            mc.set(key, str(value))
        else:
            mc.set(key, value)
    except Exception as e:
        web_logger.error('error:%s %s',  type(e), e)


# 删除单个
def del_cach(key):
    try:
        mc.delete(key)
    except Exception as e:
        web_logger.error('error:%s %s',  type(e), e)


# 删除多个
def del_mul_cach(keylist):
    try:
        mc.delete_multi(keylist)
    except Exception as e:
        web_logger.error('error:%s %s',  type(e), e)


# 数据自增1
def add_num(key):
    try:
        mc.incr(key)
    except Exception as e:
        web_logger.error('error:%s %s',  type(e), e)
