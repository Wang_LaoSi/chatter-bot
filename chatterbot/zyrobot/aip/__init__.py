# -*- coding: utf-8 -*-
"""
    aip public
"""

from .face import AipFace
from .imagecensor import AipImageCensor
from .imageclassify import AipImageClassify
from .imagesearch import AipImageSearch
from .kg import AipKg
from .nlp import AipNlp
from .ocr import AipOcr
from .speech import AipSpeech
