# -*- coding: utf-8 -*-
"""
    aip public
"""

from .bodyanalysis import AipBodyAnalysis
from .easydl import EasyDL
from .face import AipFace
from .imagecensor import AipImageCensor
from .imagecensor import AipImageCensor as AipContentCensor
from .imageclassify import AipImageClassify
from .imagesearch import AipImageSearch
from .kg import AipKg
from .nlp import AipNlp
from .ocr import AipOcr
from .speech import AipSpeech
