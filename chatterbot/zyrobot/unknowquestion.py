#encoding=UTF-8
# from __future__ import division
# from .projectsql import robotsql
# from .mysqlconn import conn
# from flask import Flask
# from flask import request
# from flask_cors import *
#
# app = Flask(__name__)
# CORS(app, supports_credentials=True)
#
# notcmpid = '无效的token！！ '
from .robotanswer import *


@app.route('/unknowquestion',methods=['GET', 'POST'])
def unknowquestion():
     global dbname
     if  not 'cmpid' in request.args or not 'begintime' in request.args \
      or not 'endtime' in request.args or not 'pagesize' in request.args \
      or not 'page' in request.args or not 'ignoretype' in request.args:
          return robotsql.backunknow(0,1,'参数不正确，请重新输入！')
     else:
          try:
               cmpid = request.args['cmpid'] #公司编码
               begintime = request.args['begintime'] #开始时间
               endtime = request.args['endtime'] #结束时间
               pagesize = request.args['pagesize'] #开始行数
               page = request.args['page'] #结束行数
               ignoretype = request.args['ignoretype'] #是否忽略
               channerlid=''
               if 'channerlid' in request.values:
                    channerlid = request.args['channerlid'] #渠道
               serviceid=''
               if 'serviceid' in request.values:
                    serviceid = request.args['serviceid'] #
               if 'dbname' in request.values:
                    dbname = request.args['dbname']
               else:
                    ip = request.host
                    dbname = robotsql.get_dbname(ip, dbname)
               unknow = robotsql.selunknowquestion(cmpid,begintime,endtime,pagesize,page,ignoretype,channerlid,serviceid,dbname,'')
             
               if unknow != conn.err:
               
                   return  robotsql.backunknow(1,2,unknow)
               else:
                   return robotsql.backunknow(0,1,'查询数据出错！')
          except Exception as e:
               print(e)
               return robotsql.backunknow(0,1,'获取无法回答的问题错误！')

@app.route('/updataignoretype',methods=['GET', 'POST'])
def updataignoretype():
     global dbname
     if  not 'sessionid' in request.args :
          return robotsql.backunknow(0,1,'参数不正确，请重新输入！')
     else:
          try:
               sessionid = request.args['sessionid'] #公司编码
               serviceid = ''
               if 'serviceid' in request.values:
                    serviceid = request.args['serviceid']
               if 'dbname' in request.values:
                    dbname = request.args['dbname']
               else:
                    ip = request.host
                    dbname = robotsql.get_dbname(ip, dbname)
               cmpid = request.args['cmpid']
               sure = robotsql.updataignore(sessionid,serviceid,cmpid,dbname,'')
               if sure != conn.err:
                   return robotsql.backunknow(1,1,'true')
               else:
                   return robotsql.backunknow(0,1,'false')
          except:
               return robotsql.backunknow(0,1,'false')
               

@app.route('/unknowquestioncount',methods=['GET', 'POST'])
def unknowquestioncount():
     global dbname
     if  not 'cmpid' in request.args or not 'begintime' in request.args \
        or not 'pagesize' in request.args  or not 'page' in request.args \
      or not 'endtime' in request.args  or not 'ignoretype' in request.args:
          return robotsql.backunknow(0,1,'参数不正确，请重新输入！')
     else:
          try:
               
               cmpid = request.args['cmpid'] #公司编码
               begintime = request.args['begintime'] #开始时间
               endtime = request.args['endtime'] #结束时间
               ignoretype = request.args['ignoretype'] #是否忽略
               pagesize = request.args['pagesize'] #开始行数
               page = request.args['page'] #结束行数
               channerlid=''
               if 'channerlid' in request.values:
                    channerlid = request.args['channerlid'] #渠道
               serviceid=''
               if 'serviceid' in request.values:
                    serviceid = request.args['serviceid'] #
               if 'dbname' in request.values:
                    dbname = request.args['dbname']
               else:
                    ip = request.host
                    dbname = robotsql.get_dbname(ip, dbname)
               sure = robotsql.selunknowquestioncount(cmpid,begintime,endtime,ignoretype,channerlid,serviceid,dbname,'')
               if sure != conn.err:
                   #allpage =(float(sure)/float(pagesize)+1)
                   allpage =(float(sure)/float(pagesize))
                   if int(allpage)==0:
                        allpage=1
                   else:
                        pa = int(sure)%int(pagesize)
                        if pa!=0:
                             allpage+=1
                   return robotsql.backunknowpage(1,1,sure,int(allpage),page)
               else:
                   return robotsql.backunknowpage(0,1,1,1,1)
          except:
               return robotsql.backunknowpage(0,1,1,1,1)


#查询行业术语
@app.route("/SelNewWords",methods=['GET', 'POST'])
def SelNewWords():
     global dbname
     if not 'cmpid' in request.args or not 'newword'in request.args \
     or not 'pagesize' in request.args  or not 'page' in request.args :
          return robotsql.backpagedata(0,1,1,0,'参数不正确')
     else:
          try:
               cmpid = request.args['cmpid'] #公司编码
               newword = request.args['newword'] #新词
               pagesize = request.args['pagesize'] #每页大小
               page = request.args['page'] #多少页
               if 'dbname' in request.values:
                    dbname = request.args['dbname']
               else:
                    ip = request.host
                    dbname = robotsql.get_dbname(ip, dbname)
               allcount = robotsql.selnewworkdatapage(cmpid ,newword ,dbname,'')
               
               allpage =(float(allcount)/float(pagesize)+1)
               return robotsql.backpagedata(1,page,int(allpage),allcount,robotsql.selnewworkdata(cmpid ,newword ,pagesize,page,'',dbname,''))
          except:
               return robotsql.backpagedata(0,1,1,0,'查询出错')
               
     
#添加行业术语
@app.route("/insertnewword",methods=['GET', 'POST'])
def insertnewword():
     global dbname
     if not 'cmpid' in request.args or not 'newword' in request.args \
     or not 'newwords' in request.args or not 'wordsize' in request.args :
          return  str(0)
     try:
          cmpid = request.args['cmpid'] #公司编码
          newword = request.args['newword'] #新词
          newwords = request.args['newwords'] #相似词
          wordsize = request.args['wordsize'] #词权重
          if 'dbname' in request.values:
               dbname = request.args['dbname']
          else:
               ip = request.host
               dbname = robotsql.get_dbname(ip, dbname)
          #1添加成功 2：已存在 0：添加失败
          return str(robotsql.insertnewwords(cmpid , newword ,newwords ,wordsize,dbname,''))
     except:
          return str(0)

#修改行业术语
@app.route("/updatenewword",methods=['GET', 'POST'])
def updatenewword():
     global dbname
     if not 'wordid' in request.args or not 'newword' in request.args \
     or not 'newwords' in request.args or not 'wordsize' in request.args \
      or  not 'cmpid' in request.args :
          return  str(0)
     try:
          cmpid = request.args['cmpid'] #公司编码
          wordid = request.args['wordid'] #词编码
          newword = request.args['newword'] #新词
          newwords = request.args['newwords'] #相似词
          wordsize = request.args['wordsize'] #词权重
          if 'dbname' in request.values:
               dbname = request.args['dbname']
          else:
               ip = request.host
               dbname = robotsql.get_dbname(ip, dbname)
          return str(robotsql.updatanewwords(cmpid,wordid , newword ,newwords ,wordsize,dbname,''))
          
     except:
          return str(0)          
     
#查询行业术语详情
@app.route("/selnewwordinfo",methods=['GET', 'POST'])
def selnewwordinfo():
     global dbname
     if not 'wordid' in request.args \
      or  not 'cmpid' in request.args :
          return  robotsql.backdata(0,'')
     try:
          cmpid = request.args['cmpid'] #公司编码
          wordid = request.args['wordid'] #词编码
          if 'dbname' in request.values:
               dbname = request.args['dbname']
          else:
               ip = request.host
               dbname = robotsql.get_dbname(ip, dbname)
          return robotsql.backdata(1,robotsql.selnewworkdata(cmpid ,'' ,'','',wordid,dbname,''))
          
     except:
          return robotsql.backdata(0,'')


#删除行业术语详情
@app.route("/delnewword",methods=['GET', 'POST'])
def delnewword():
     global dbname
     if not 'wordid' in request.args \
     or  not 'cmpid' in request.args :
          return  str(0)
     try:
          cmpid = request.args['cmpid'] #公司编码
          wordid = request.args['wordid'] #词编码
          if 'dbname' in request.values:
               dbname = request.args['dbname']
          else:
               ip = request.host
               dbname = robotsql.get_dbname(ip, dbname)
          return str(robotsql.delnewwordsql(cmpid ,wordid,dbname,''))
          
     except:
          return str(0)


#智能学习
@app.route("/Intelligentlearning",methods=['GET', 'POST'])
def Intelligentlearning():
     global dbname
     if not 'quesid' in request.args or  not 'token' in request.args\
     or  not 'cid' in request.args  or  not 'learnquestion' in request.args :
          return  str(0)
     try:
          quesid = request.args['quesid'] #问题编码
          learnquestion = request.args['learnquestion'] #学习到的内容
          token = request.args['token'] #token
          cid = request.args['cid'] #token
          if 'dbname' in request.values:
               dbname = request.args['dbname']
          else:
               ip = request.host
               dbname = robotsql.get_dbname(ip, dbname)
          return str(robotsql.addrobotlearning(token ,quesid,learnquestion,cid,dbname))
          
     except:
          return str(0)
     
     

#app.run(host="", port=8008, debug=True)
