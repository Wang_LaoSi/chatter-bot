# file_name=init_celery.py
# coding: utf-8
from celery import Celery

BROKER_URL = 'amqp://lzj:Haitang110@47.94.89.73:5672//'
BACKEND_URL = 'amqp://lzj:Haitang110@47.94.89.73:5672//'

# Add tasks here
CELERY_IMPORTS = (
    'try',
)

celery = Celery('uploadfile',
                broker=BROKER_URL,
                backend=BACKEND_URL,
                include=CELERY_IMPORTS, )

celery.conf.update(
    CELERY_ACKS_LATE=True,
    CELERY_ACCEPT_CONTENT=['pickle', 'json'],
    CELERYD_FORCE_EXECV=True,
    CELERYD_MAX_TASKS_PER_CHILD=500,
    BROKER_HEARTBEAT=0,
)
