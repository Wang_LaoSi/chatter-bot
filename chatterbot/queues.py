class FixedSizeQueue(object):
    """
    This is a data structure like a queue.
    Only a fixed number of items can be added.
    Once the maximum is reached, when a new item is
    added the oldest item in the queue will be removed.

    这是一个类似队列的数据结构。
    只能添加固定数量的项。
    一旦达到最大值，一个新的项目时，
    在队列中添加的最古老的项目将被删除。
    """

    def __init__(self, maxsize=10):
        self.maxsize = maxsize
        self.queue = []

    def append(self, item):
        """
        Append an element at the end of the queue.
        在队列的末端追加一个元素。
        """
        if len(self.queue) == self.maxsize:
            # Remove an element from the top of the list
            #从列表顶部移除一个元素
            self.queue.pop(0)

        self.queue.append(item)
    """
    定义当被 len() 调用时的行为
    """
    def __len__(self):
        return len(self.queue)
    """
    定义获取容器中指定元素的行为，相当于 self[key]
    """
    def __getitem__(self, index):
        return self.queue[index]

    """
    定义当使用成员测试运算符（in 或 not in）时的行为
    """
    def __contains__(self, item):
        """
        Check if an element is in this queue.
        检查如果一个元素是在这排队。
        """
        return item in self.queue

    def empty(self):
        """
        Return True if the queue is empty, False otherwise.
        如果队列为空，返回true，否则为false。
        """
        return len(self.queue) == 0

    def peek(self):
        """
        Return the most recent item put in the queue.
        返回最近的项目放在队列。
        """
        if self.empty():
            return None
        return self.queue[-1]

    def flush(self):
        """
        Remove all elements from the queue.
        从队列中删除所有元素。
        """
        self.queue = []


class ResponseQueue(FixedSizeQueue):
    """
    An extension of the FixedSizeQueue class with
    utility methods to help manage the conversation.

    一个扩展的fixedsizequeue类
    实用的方法来帮助管理会话。
    """

    def get_last_response_statement(self):
        """
        Return the last statement that was received.
        返回收到的最后一条语句。
        """
        previous_interaction = self.peek()
        if previous_interaction:
            # Return the output statement返回输出语句
            return previous_interaction[1]
        return None

    def get_last_input_statement(self):
        """
        Return the last response that was given.
        返回最后一个反应是给予。
        """
        previous_interaction = self.peek()
        if previous_interaction:
            # Return the input statement 返回输入语句
            return previous_interaction[0]
        return None
