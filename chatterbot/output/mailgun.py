from __future__ import unicode_literals

from .output_adapter import OutputAdapter


class Mailgun(OutputAdapter):

    def __init__(self, **kwargs):
        super(Mailgun, self).__init__(**kwargs)

        # Use the bot's name for the name of the sender
        self.name = kwargs.get('name')
        self.from_address = kwargs.get('mailgun_from_address')
        self.api_key = kwargs.get('mailgun_api_key')
        self.endpoint = kwargs.get('mailgun_api_endpoint')
        self.recipients = kwargs.get('mailgun_recipients')

    def send_message(self, subject, text, from_address, recipients):
        """
        * subject: Subject of the email.
        * text: Text body of the email.
        * from_email: The email address that the message will be sent from.
        * recipients: A list of recipient email addresses.

        *主题：电子邮件的主题。
        *文本的电子邮件正文。
        * from_email：该消息将被发送的电子邮件地址。
        *收件人：收件人的电子邮件地址列表。
        """
        import requests

        return requests.post(
            self.endpoint,
            auth=('api', self.api_key),
            data={
                'from': '%s <%s>' % (self.name, from_address),
                'to': recipients,
                'subject': subject,
                'text': text
            })

    def process_response(self, statement, session_id=None):
        """
        Send the response statement as an email.
        作为电子邮件发送响应的声明。
        """
        subject = 'Message from %s' % (self.name)

        self.send_message(
            subject,
            statement.text,
            self.from_address,
            self.recipients
        )

        return statement
