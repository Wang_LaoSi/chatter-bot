from chatterbot.adapters import Adapter


class OutputAdapter(Adapter):
    """
    A generic class that can be overridden by a subclass to provide extended
    functionality, such as delivering a response to an API endpoint.

    泛型类可以重写的子类提供扩展
    功能，如提供一个API端点的响应。
    """

    def process_response(self, statement, session_id=None):
        """
        Override this method in a subclass to implement customized functionality.

        :param statement: The statement that the chat bot has produced in response to some input.

        :param session_id: The unique id of the current chat session.

        :returns: The response statement.
        在子类中覆盖这个方法来实现定制功能。
        ：参数声明：声明，聊天机器人已经在回应一些投入生产。
        session_id：参数：当前聊天会话的ID。
        返回：响应语句。
        """
        return statement
