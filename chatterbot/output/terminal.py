from __future__ import unicode_literals

from .output_adapter import OutputAdapter


class TerminalAdapter(OutputAdapter):
    """
    A simple adapter that allows ChatterBot to
    communicate through the terminal.
    一个简单的适配器，可以让聊天机器人来
    通过终端通信。
    """

    def process_response(self, statement, session_id=None):
        """
        Print the response to the user's input.
        打印用户输入的响应。
        """
        print(statement.text)
        return statement.text
