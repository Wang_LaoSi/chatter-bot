"""
Filters set the base query that gets passed to the storage adapter.
滤波器组被传递给存储适配器的基础查询。
"""


class Filter(object):
    """
    A base filter object from which all other
    filters should be subclassed.
    一个基础的过滤器对象从所有其他的
    过滤器应继承。
    """

    def filter_selection(self, chatterbot, session_id):
        """
        Because this is the base filter class, this method just
        returns the storage adapter's base query. Other filters
        are expected to override this method.
        因为这是基本的过滤器类，这种方法只
        返回存储适配器的基本查询。其他的过滤器
        将重写此方法。
        """
        return chatterbot.storage.base_query


class RepetitiveResponseFilter(Filter):
    """
    A filter that eliminates possibly repetitive responses to prevent
    a chat bot from repeating statements that it has recently said.
    一个过滤器，以防止可能消除重复的反应
    从重复的语句，它最近一个聊天机器人说。
    """

    def filter_selection(self, chatterbot, session_id):

        session = chatterbot.conversation_sessions.get(session_id)

        if session.conversation.empty():
            return chatterbot.storage.base_query

        text_of_recent_responses = []

        for statement, response in session.conversation:
            text_of_recent_responses.append(response.text)

        query = chatterbot.storage.base_query.statement_text_not_in(
            text_of_recent_responses
        )

        return query
