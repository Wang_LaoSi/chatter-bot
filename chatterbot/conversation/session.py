import uuid
#from chatterbot.queues import ResponseQueue
from ..queues import ResponseQueue



class Session(object):
    """
    A session is an ordered collection of statements
    that are related to each other.

    会话是有序的语句集合。这是相互关联的
    """

    def __init__(self):
        # A unique identifier for the chat session
        self.uuid = uuid.uuid1()
        self.id_string = str(self.uuid)
        self.id = str(self.uuid)

        # The last 10 statement inputs and outputs
        self.conversation = ResponseQueue(maxsize=10)


class ConversationSessionManager(object):
    """
    Object to hold and manage multiple chat sessions.
    对象来保存和管理多个聊天会话。
    """

    def __init__(self):
        self.sessions = {}

    def new(self):
        """
        Create a new conversation.
        创建新会话。
        """
        session = Session()

        self.sessions[session.id_string] = session

        return session

    def get(self, session_id, default=None):
        """
        Return a session given a unique identifier.
        返回给定唯一标识符的会话。
        """
        return self.sessions.get(str(session_id), default)

    def update(self, session_id, conversance): #conversance熟悉，精通;
        """
        Add a conversance to a given session if the session exists.
        添加一个精通某一会议如果会话存在。
        """
        session_id = str(session_id)
        if session_id in self.sessions:
            self.sessions[session_id].conversation.append(conversance)
