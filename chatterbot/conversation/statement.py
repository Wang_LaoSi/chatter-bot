# -*- coding: utf-8 -*-
from .response import Response
from datetime import datetime


class Statement(object):
    """
    A statement represents a single spoken entity, sentence or
    phrase that someone can say.
    声明中表示一个单一的口语句子或实体，
    有人能说的话。
    """

    def __init__(self, text, **kwargs):

        # Try not to allow non-string types to be passed to statements

        #尽量不允许非字符串被传递到报表
        try:
            text = str(text)
        except UnicodeEncodeError:
            pass

        self.text = text
        self.in_response_to = kwargs.pop('in_response_to', [])

        # The date and time that this statement was created at
        #日期和时间，这项声明是在
        self.created_at = kwargs.pop('created_at', datetime.now())

        self.extra_data = kwargs.pop('extra_data', {})

        # This is the confidence with which the chat bot believes
        # this is an accurate response. This value is set when the
        # statement is returned by the chat bot.
        #这是信心的聊天机器人认为这是一个准确的反应
        #。设置此值时
        #声明的聊天机器人返回。
        self.confidence = 0

        self.storage = None

    def __str__(self):
        return self.text

    def __repr__(self):
        return '<Statement text:%s>' % (self.text)

    def __hash__(self):
        return hash(self.text)

    def __eq__(self, other):
        if not other:
            return False

        if isinstance(other, Statement):
            return self.text == other.text

        return self.text == other

    def save(self):
        """
        Save the statement in the database.
        将语句保存在数据库中。
        """
        self.storage.update(self)

    def add_extra_data(self, key, value):
        """
        This method allows additional data to be stored on the statement object.

        Typically this data is something that pertains just to this statement.
        For example, a value stored here might be the tagged parts of speech for
        each word in the statement text.

            - key = 'pos_tags'
            - value = [('Now', 'RB'), ('for', 'IN'), ('something', 'NN'), ('different', 'JJ')]

        :param key: The key to use in the dictionary of extra data.
        :type key: str

        :param value: The value to set for the specified key.
        此方法允许将其他数据存储在语句对象上。
        通常，此数据与此语句相关。
        例如，这里存储的值可能是标记的语音部分。
        语句文本中的每个词。
        键=“pos_tags”
        价值= [（“'，'包'），（“”，“”），（“”，“神经网络”），（'different，JJ的）]
        ：参数键：使用额外的数据字典的关键。
        键入密钥：STR
        ：参数值：设置为指定的键的值。
        """
        self.extra_data[key] = value

    def add_response(self, response):
        """
        Add the response to the list of statements that this statement is in response to.
        If the response is already in the list, increment the occurrence count of that response.

        :param response: The response to add.
        :type response: `Response`

        向该语句响应的语句列表添加响应。
        如果响应已经在列表中，则增加该响应的发生计数。
        ：参数响应：添加的响应。
        ：类型响应：“响应”
        """
        if not isinstance(response, Response):
            raise Statement.InvalidTypeException(
                'A {} was recieved when a {} instance was expected'.format(
                    type(response),
                    type(Response(''))
                )
            )

        updated = False
        for index in range(0, len(self.in_response_to)):
            if response.text == self.in_response_to[index].text:
                self.in_response_to[index].occurrence += 1
                updated = True

        if not updated:
            self.in_response_to.append(response)

    def remove_response(self, response_text):
        """
        Removes a response from the statement's response list based
        on the value of the response text.

        :param response_text: The text of the response to be removed.
        :type response_text: str
        从语句的响应列表中删除响应
        论回应文本的价值。
        ：参数response_text：被删除的文本的反应。
        ：型response_text：STR
        """
        for response in self.in_response_to:
            if response_text == response.text:
                self.in_response_to.remove(response)
                return True
        return False

    def get_response_count(self, statement):
        """
        Find the number of times that the statement has been used
        as a response to the current statement.

        :param statement: The statement object to get the count for.
        :type statement: `Statement`

        :returns: Return the number of times the statement has been used as a response.
        :rtype: int
        查找语句被使用的次数。
        作为对目前声明的回应。
        ：参数声明：声明对象获得数。
        type语句：'语句'
        返回：返回语句被用作响应的次数。
        ：关系类型：int
        """
        for response in self.in_response_to:
            if statement.text == response.text:
                return response.occurrence

        return 0

    def serialize(self):
        """
        :returns: A dictionary representation of the statement object.
        :rtype: dict
        返回：语句对象的字典表示形式。
        ：R型：字典
        """
        data = {}

        data['text'] = self.text
        data['in_response_to'] = []
        data['created_at'] = self.created_at
        data['extra_data'] = self.extra_data

        for response in self.in_response_to:
            data['in_response_to'].append(response.serialize())

        return data

    @property
    def response_statement_cache(self):
        """
        This property is to allow ChatterBot Statement objects to
        be swappable with Django Statement models.
        此属性允许Chatterbot语句对象
        可插拔的Django报表模型。
        """
        return self.in_response_to

    class InvalidTypeException(Exception):

        def __init__(self, value='Recieved an unexpected value type.'):
            self.value = value

        def __str__(self):
            return repr(self.value)
