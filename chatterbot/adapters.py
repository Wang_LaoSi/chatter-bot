import logging


class Adapter(object):
    """
    A superclass for all adapter classes.
    为所有适配器的父类。
    """

    def __init__(self, **kwargs):
        self.logger = kwargs.get('logger', logging.getLogger(__name__))
        self.chatbot = None

    def set_chatbot(self, chatbot):
        """
        Gives the adapter access to an instance of the ChatBot class.
        给出了适配器访问的聊天类的一个实例。
        """
        self.chatbot = chatbot

    class AdapterMethodNotImplementedError(NotImplementedError):
        """
        An exception to be raised when an adapter method has not been implemented.
        Typically this indicates that the developer is expected to implement the
        method in a subclass.
        未实现适配器方法时要引发的异常。
        通常，这表明开发人员应该实现
        子类中的方法。
        """

        def __init__(self, message=None):
            """
            Set the message for the esception.
            为esception设置消息。
            """
            if not message:
                message = 'This method must be overridden in a subclass method.' #必须在子类方法中重写此方法。
            self.message = message

        def __str__(self):
            return self.message

    class InvalidAdapterTypeException(Exception):
        """
        An exception to be raised when an adapter of an unexpected class type is recieved.
        当接收到意外类类型的适配器时会引发异常。
        """
        pass
