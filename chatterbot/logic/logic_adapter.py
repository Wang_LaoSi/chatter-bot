from __future__ import unicode_literals

# from chatterbot.adapters import Adapter
# from chatterbot.utils import import_module
from ..adapters import Adapter
from ..utils import import_module


class LogicAdapter(Adapter):
    """
    This is an abstract class that represents the interface
    that all logic adapters should implement.

    这是一个表示接口的抽象类。
    所有逻辑适配器都应该实现。
    """

    def __init__(self, **kwargs):
        super(LogicAdapter, self).__init__(**kwargs)
        from chatterbot.comparisons import levenshtein_distance
        from chatterbot.response_selection import get_first_response

        # Import string module parameters输入字符串模块参数
        if 'statement_comparison_function' in kwargs:
            import_path = kwargs.get('statement_comparison_function')
            if isinstance(import_path, str):
                kwargs['statement_comparison_function'] = import_module(import_path)

        if 'response_selection_method' in kwargs:
            import_path = kwargs.get('response_selection_method')
            if isinstance(import_path, str):
                kwargs['response_selection_method'] = import_module(import_path)

        # By default, compare statements using Levenshtein distance默认情况下，使用Levenshtein距离比较报表
        self.compare_statements = kwargs.get(
            'statement_comparison_function',
            levenshtein_distance
        )

        # By default, select the first available response默认情况下，选择第一个可用的响应。
        self.select_response = kwargs.get(
            'response_selection_method',
            get_first_response
        )

        self.confidence_threshold = kwargs.get(
            'threshold',
            0
        )

        # 定义会话编码,公司编码,引用模块
        self.cid = kwargs.get(
            'cid', '-1'
        )
        self.cmpid = kwargs.get(
            'cmpid', '-1'
        )
        self.serviceid = kwargs.get(
            'serviceid', '-1'
        )
        self.default_response = kwargs.get(
            'default_response', ''
        )
        self.askid = kwargs.get(
            'askid', '-1'
        )
        self.operation = kwargs.get(
            'operation', '-1'
        )
        self.industryid = kwargs.get(
            'industryid', None
        )
        self.session_data = kwargs.get(
            'session_data', ''
        )
        self.session_sort = kwargs.get(
            'session_sort', ''
        )
        self.dbname = kwargs.get(
            'dbname', ''

        )

    def get_initialization_functions(self):
        """
        Return a dictionary of functions to be run once when the chat bot is instantiated.
        返回一个字典的功能是运行时实例化的聊天机器人。
        """
        return self.compare_statements.get_initialization_functions()

    def initialize(self):
        for function in self.get_initialization_functions().values():
            function()

    def can_process(self, statement):
        """
        A preliminary check that is called to determine if a
        logic adapter can process a given statement. By default,
        this method returns true but it can be overridden in
        child classes as needed.

        :rtype: bool
        初步检查来确定
        逻辑适配器可以处理一个给定的语句。默认情况下，
        此方法返回true，但它可以被重写
        根据需要的子类。
        ：R型：bool
        """
        return True

    def process(self, statement):
        """
        Override this method and implement your logic for selecting a response to an input statement.

        A confidence value and the selected response statement should be returned.
        The confidence value represents a rating of how accurate the logic adapter
        expects the selected response to be. Confidence scores are used to select
        the best response from multiple logic adapters.

        The confidence value should be a number between 0 and 1 where 0 is the
        lowest confidence level and 1 is the highest.

        :param statement: An input statement to be processed by the logic adapter.
        :type statement: Statement

        :rtype: Statement
        重写此方法，选择一个输入语句的响应你的逻辑实现。
        一个自信的价值和选择的回应声明应退还。
        信心值表示评级如何精确的逻辑适配器
        希望选择的反应是。信心分数是用来选择
        从多个逻辑适配器最好的回应。
        信心值应该是0和1之间的数字，0是
        最低和最高是1的置信水平。
        ：参数声明：输入语句的逻辑适配器处理。
        类型声明：声明：
        R型：声明：
        """
        raise self.AdapterMethodNotImplementedError()

    @property
    def class_name(self):
        """
        Return the name of the current logic adapter class.
        This is typically used for logging and debugging.
        返回当前逻辑适配器类的名称。
        这通常是用于日志记录和调试。
        """
        return str(self.__class__.__name__)

    class EmptyDatasetException(Exception):

        def __init__(self, value='An empty set was received when at least one statement was expected.'):
            self.value = value

        def __str__(self):
            return repr(self.value)
