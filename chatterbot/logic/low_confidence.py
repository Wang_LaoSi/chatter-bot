from __future__ import unicode_literals

from .best_match import BestMatch
# from chatterbot.conversation import Statement
from ..conversation import Statement


class LowConfidenceAdapter(BestMatch):
    """
    Returns a default response with a high confidence
    when a high confidence response is not known.
    返回一个具有高置信度的默认响应
    当一个高可信度的反应是不知道。
    """

    def __init__(self, **kwargs):
        super(LowConfidenceAdapter, self).__init__(**kwargs)

        self.confidence_threshold = kwargs.get('threshold', 0.65)
        default_response = kwargs.get(
            'default_response', "I'm sorry, I do not understand."
        )

        self.default_response = Statement(text=default_response)

    def process(self, input_statement):
        """
        Return a default response with a high confidence if
        a high confidence response is not known.
        回一个高可信度的默认响应如果
        高置信度的反应是不知道。
        """
        # Select the closest match to the input statement选择最匹配的输入语句
        closest_match = self.get(input_statement)
        # Confidence should be high only if it is less than the threshold只有当信心低于门槛时，信心才会高。

        if closest_match.confidence < self.confidence_threshold:
            self.default_response.confidence = 1
        else:
            self.default_response.confidence = 0
        return self.default_response
