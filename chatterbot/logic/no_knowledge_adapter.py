from __future__ import unicode_literals

from .logic_adapter import LogicAdapter


class NoKnowledgeAdapter(LogicAdapter):
    """
    This is a system adapter that is automatically added
    to the list of logic adapters durring initialization.
    This adapter is placed at the beginning of the list
    to be given the highest priority.
    这是一个系统的适配器，自动添加
    对逻辑适配器在初始化列表。
    该适配器放在列表的开始
    给予最高的优先级。
    """

    def process(self, statement):
        """
        If there are no known responses in the database,
        then a confidence of 1 should be returned with
        the input statement.
        Otherwise, a confidence of 0 should be returned.
        如果没有已知的反应数据库，
        然后1的把握应返回
        输入语句。
        否则，一个自信的0应返还。
        """

        if self.chatbot.storage.count():
            statement.confidence = 0
        else:
            statement.confidence = 1

        return statement
