# -*-coding:utf-8-*-
from __future__ import unicode_literals

import datetime
import re
import sys
import time

from .logic_adapter import LogicAdapter
from ..zyrobot.mysqlconn import conn
from logging import getLogger

web_logger = getLogger('web_logger')


class newStatementclass(object):

    def __init__(self, text, **kwargs):

        try:
            text = str(text)
        except UnicodeEncodeError:
            pass

        self.text = text
        self.confidence = 0


class BestMatchsession(LogicAdapter):
    """
    A logic adater that returns a response based on known responses to
    the closest matches to the input statement.

    """

    def get(self, input_statement):
        """
        Takes a statement string and a list of statement strings.
        Returns the closest matching statement from the list.


        """
        statement_list = self.chatbot.storage.get_response_statements()
        #   print(statement_list)
        # print(statement_list)
        # if not statement_list:
        #     if self.chatbot.storage.count():
        #         # Use a randomly picked statementʹ
        #         self.logger.info(
        #             'No statements have known responses. ' +
        #             'Choosing a random response to return.'
        #         )
        #         random_response = self.chatbot.storage.get_random()
        #         random_response.confidence = 0
        #         return random_response
        #     else:
        #         raise self.EmptyDatasetException()

        # closest_match = input_statement
        # closest_match.confidence = 0

        closest_match = input_statement
        # closest_match = {'text':input_statement,'confidence':0}

        # closest_match.text = self.default_response
        closest_match.confidence = self.confidence_threshold

        # Find the closest matching known statement

        for statement in statement_list:
            # print("statementstatementstatementstatementstatementstatement")
            # print('statement',statement)
            # print("statementstatementstatementstatementstatementstatement")
            confidence = self.compare_statements(statement, input_statement)

            if confidence > closest_match.confidence:
                web_logger.info('cid:%s confidence:%s statement:%s input_statement:%s', self.cid, confidence, statement,
                                input_statement)
                statement.confidence = confidence
                closest_match = statement

        return closest_match

    def selhavebackanswerid(self, cid, serviceid):
        sql = "select r.answerid from zy_rcustomerquestion q,zy_robotanswer r  \
        where q.cid='" + str(cid) + "' and q.serviceid='" + str(
            serviceid) + "' and q.sessionid=r.sessionid and r.answerid is not NULL GROUP BY  answerid"
        answeridgroup = conn.mysql_sel(sql, self.dbname, cid)
        return answeridgroup

    def can_process(self, statement):
        """
        Check that the chatbot's storage adapter is available to the logic
        adapter and there is at least one statement in the database.

        """
        # return self.chatbot.storage.count()
        return True

    def fuzzyquery(self, cmpid, serviceid, text, industryid):
        sql = "select  q.quesid,q.keywords from zt_know_question q where q.cmpid='" + str(
            cmpid) + "' and q.serviceid='" + str(serviceid) + "' and q.keywords!='' and q.state=1 "

        keywords = conn.mysql_sel(sql, self.dbname, self.cid)
        return keywords

    def selanswerid(self, quesid):
        sql = "select a.answerid from zt_know_answer a where a.quesid='" + str(quesid) + "' ORDER BY a.sort  LIMIT 0,1"
        answerid = conn.mysql_selrowid(sql, self.dbname, self.cid)
        return answerid

    def tracommfuzzyquery(self, cmpid, serviceid, text, industryid):
        sql = "select  q.quesid,q.keywords from zt_know_question q where  q.keywords!='' and q.state=1 "

        if industryid != None and industryid != "":
            sql = sql + " and industryid= '" + str(industryid) + "'"
        keywords = conn.mysql_sel(sql, self.dbname, self.cid)

        return keywords

    def selfuzzyqueryanswer(self, keywork, cmpid, serviceid, dbname, cid):
        sql = "select q.answerid from zt_know_question q where q.cmpid='" + str(cmpid) + "' and" \
                                                                                         " q.serviceid='" + str(
            serviceid) + "' and  FIND_IN_SET( '" + str(keywork) + "',q.keywords) ORDER BY q.createtime DESC LIMIT 0,1"
        keywords = conn.mysql_selrowid(sql, dbname, cid)
        return keywords

    def insertquestion(self, answerid, cid, cmpid):
        sql = "select a.quesid from zt_know_answer a where a.answerid='" + str(answerid) + "' "
        questionid = conn.mysql_selrowid(sql, self.dbname, cid)
        if questionid == None or questionid == '':
            sql = "select q.quesid from zt_know_question q where q.answerid='" + str(answerid) + "'"
            questionid = conn.mysql_selrowid(sql, self.dbname, cid)

        insertcount = str(0)

        if questionid != conn.err:
            sqlinsert = "INSERT into zt_cmp_robot_respond(cmpid,cid,questionid,session_data,session_sort) VALUES ('" + str(
                cmpid) + "','" + str(cid) + "','" + str(
                questionid) + "','" + self.session_data + "','" + self.session_sort + "')"

            insertcount = conn.mysql_insert(sqlinsert, self.dbname, cid)

        return str(insertcount)

    def ifhoodlemobole(self, askid):
        sql = "select a.attitude from zt_know_hoodlemobile_answer a where a.HoodleMobileid='" + str(
            askid) + "' ORDER BY a.attitude "
        attitudelist = conn.mysql_sel(sql, self.dbname, self.cid)
        attitudelist = attitudelist + ((6,), (7,), (11,))
        web_logger.info('cid:%s attitudelist:%s', self.cid, attitudelist)
        return attitudelist

    # 查询技能
    def selskill(self, serviceid):
        sql = "SELECT s.skillid,s.keywords from zt_skill s  where s.state=1 and keywords!='' and keywords is not null and s.serviceid='" + str(
            serviceid) + "'"
        keywords = conn.mysql_sel(sql, self.dbname, self.cid)

        return keywords

    def backanswer(self, statement_list, closest_match, input_statement):
        if statement_list != None and len(statement_list) != 0:
            closest_match.confidence = 0.85
            maxsocre = 0
            maxtext = ''
            for statement in statement_list:
                socre = 1
                if statement.text.count('$') >= 1:
                    socre = statement.text.split('$')[-1]
                    newstatement = statement.text[:statement.text.rfind('$')]
                    web_logger.debug('newstatement:%s socre:%s input_statement:%s maxtext:%s', newstatement, socre,
                                    input_statement, statement.text)
                    if newstatement.strip() != "" and newstatement.strip() == str(input_statement).strip():
                        maxtext = statement.text
                        maxsocre = 1

                    elif newstatement.strip() != "" and newstatement.strip() in str(input_statement):
                        if maxsocre != 1:
                            if float(socre) == float(maxsocre):
                                if len(statement.text) > len(maxtext):
                                    maxtext = statement.text
                                    maxsocre = socre
                                    web_logger.debug('socre=maxsocre cid:%s maxtext:%s maxsocre:%s', self.cid,
                                                    statement.text, maxsocre)
                            elif float(socre) > float(maxsocre):
                                maxtext = statement.text
                                maxsocre = socre
                                web_logger.debug('socre>maxsocre cid:%s maxtext:%s socre:%s maxsocre:%s ', self.cid,
                                                maxtext, socre, maxsocre)
                else:
                    confidence = self.compare_statements(statement, input_statement)
                    if confidence > closest_match.confidence:
                        web_logger.info(
                            'confidence >closest_match.confidence cid:%s confidence:%s statement:%s input_statement:%s ',
                            self.cid, confidence, statement, input_statement)

                        statement.confidence = confidence
                        closest_match = statement

            if maxsocre != 0 and closest_match.confidence != 1:
                response_list = self.chatbot.storage.filter(
                    in_response_to__contains=maxtext
                )
                if len(response_list) != 0:
                    response = self.select_response(input_statement, response_list)
                    response.confidence = maxsocre

                    if response.text != self.default_response:
                        response.text = 'attitude' + response.text

                    return response

            response_list = self.chatbot.storage.filter(
                in_response_to__contains=closest_match.text
            )
            if len(response_list) != 0:
                response = self.select_response(input_statement, response_list)
                response.confidence = closest_match.confidence

                if response.text != self.default_response:
                    response.text = 'attitude' + response.text

                return response

    def selmongodbdata(self, data, tablename):
        statement_data = self.chatbot.storage.get_response_statements_database(data, tablename)
        if len(statement_data) != 0:
            statement_list = statement_data

            return statement_list

    # 查询公共行业问题
    def selindustrycomm(self, industryid, closest_match, input_statement):
        ifhavetable = self.chatbot.storage.seltable('industry' + str(industryid), '')

        if str(ifhavetable) != '0':
            statement_data = self.selmongodbdata('industry' + str(industryid), '')
            web_logger.info('cid:%s industry:%s', self.cid, industryid)
            backanswerdata = self.backanswer(statement_data, closest_match, input_statement)
            if backanswerdata != None:
                alltext = backanswerdata.text[8:]
                allconfidence = backanswerdata.confidence
                return (allconfidence, alltext)

    def attitudejudge(self, closest_match, input_statement):

        alltext = ''
        allconfidence = 0
        updatelist = 0
        attitudelist = self.ifhoodlemobole(self.askid)
        if attitudelist == ((5,), (6,), (7,), (11,)):
            updatelist = 1

            attitudelist += ((1,), (2,), (3,))
        ifhavetable = self.chatbot.storage.seltable('database' + str(self.cmpid) + '-HoodleMobile' + str(self.askid),
                                                    str(self.dbname) + 'database' + str(self.cmpid))

        if str(len(attitudelist)) != '0':
            if str(ifhavetable) != '0':
                statement_data = self.selmongodbdata('database' + str(self.cmpid) + '-HoodleMobile' + str(self.askid),
                                                     str(self.dbname) + 'database' + str(self.cmpid))
                backanswerdata = self.backanswer(statement_data, closest_match, input_statement)

                if backanswerdata != None:
                    alltext = backanswerdata.text
                    allconfidence = backanswerdata.confidence
                    web_logger.debug('alltext:%s', alltext)
            if allconfidence != 1:
                web_logger.debug('attitudelist:%s', attitudelist)
                for attitude in attitudelist:
                    web_logger.debug('attitude:%s', attitude)
                    statement_data = self.selmongodbdata('answertype' + str(attitude[0]), '')
                    backanswerdata = self.backanswer(statement_data, closest_match, input_statement)
                    if backanswerdata != None:
                        web_logger.debug('attitude:%s backanswerdata.confidence:%s allconfidence:%s '
                                        'backanswerdata.text:%s alltext:%s',
                                        attitude, backanswerdata.confidence, allconfidence,
                                        backanswerdata.text, alltext)
                        if backanswerdata.confidence != 1:
                            # print('ggggg')
                            if float(backanswerdata.confidence) == float(allconfidence):
                                if list(backanswerdata.text)[-1] > list(alltext)[-1]:
                                    allconfidence = backanswerdata.confidence
                                    alltext = backanswerdata.text
                            elif float(backanswerdata.confidence) > float(allconfidence):
                                allconfidence = backanswerdata.confidence
                                alltext = backanswerdata.text
                        else:
                            allconfidence = backanswerdata.confidence
                            alltext = backanswerdata.text
        if updatelist == 1:
            if alltext[:-1] == 'attitude' or alltext[:-2] == 'attitude':
                if str(alltext[8:]) == '1' or str(alltext[8:]) == '2' or str(alltext[8:]) == '3':
                    alltext = 'attitude5'

        web_logger.debug('allconfidence:%s alltext:%s', allconfidence, alltext)
        return (allconfidence, alltext)

    def groo_keywork(self, input_statement, response):
        keywords = self.fuzzyquery(self.cmpid, self.serviceid, input_statement, '')
        if len(keywords) != 0:
            quesid = ''
            maxkeywordtext = ''
            for i in keywords:
                worklist = []
                try:
                    newzheng = i[1].replace(",", "|")
                    pipe_word = re.compile(newzheng)
                    worklist = re.findall(pipe_word, input_statement.text)
                except:
                    web_logger.info('cid:%s keyword:%s', self.cid, i)
                if len(worklist) != 0:
                    worklen = 0
                    workend = ''
                    for work in worklist:
                        if len(work) > worklen:
                            worklen = len(work)
                            workend = work

                    if maxkeywordtext != "":
                        if worklen > len(maxkeywordtext):
                            quesid = i[0]
                            maxkeywordtext = workend
                    else:
                        quesid = i[0]
                        maxkeywordtext = workend
                    web_logger.info('cid:%s maxkeywordtext:%s', self.cid, maxkeywordtext)
            if maxkeywordtext != "":
                answerid = self.selanswerid(quesid)
                web_logger.info('cid:%s quesid:%s maxkeywordtext:%s answerid:%s', self.cid, quesid, maxkeywordtext,
                                answerid)
                if answerid != "":
                    response.text = 'A' + str(answerid)
                    response.confidence = 1
                    self.insertquestion(answerid, self.cid, self.cmpid)
                    web_logger.info('cid:%s response.text:%s', self.cid, response.text)
                    return response

    # 态度正则查询
    def sel_reg_data(self):
        sql = "SELECT a.regwords,a.attitude from zt_know_hoodlemobile_answer a where a.HoodleMobileid='" + str(
            self.askid) + "' and a.regwords!='' and a.regwords is not NULL"
        regword = conn.mysql_sel(sql, self.dbname, self.cid)
        return regword

    def pipe_reg_attitue(self, input_statement, response):
        regwordslist = self.sel_reg_data()
        # #(regwordslist)
        attitudedata = ''
        if len(regwordslist) != 0:
            maxkeywordtext = ""
            for regwords in regwordslist:
                worklist = []
                try:
                    # print(0,regwords)
                    regdata = regwords[0].replace(",", "|")
                    # print(regdata)
                    pipe_word = re.compile(regdata)
                    # print(2,pipe_word)
                    worklist = re.findall(pipe_word, input_statement.text)
                except:
                    web_logger.info('cid:%s regwords:%s', self.cid, regwords)
                if len(worklist) != 0:
                    worklen = 0
                    workend = ''
                    for work in worklist:
                        if len(work) > worklen:
                            worklen = len(work)
                            workend = work
                    if maxkeywordtext != "":
                        if worklen > len(maxkeywordtext):
                            attitudedata = regwords[1]
                            maxkeywordtext = workend
                    else:
                        attitudedata = regwords[1]
                        maxkeywordtext = workend

            if maxkeywordtext != "":
                web_logger.info('cid:%s attitude:%s maxkeywordtext:%s', self.cid, attitudedata, maxkeywordtext)
                response.text = 'attitude' + str(attitudedata)
                response.confidence = 1
                return response

    def process(self, input_statement):
        # print('sssd')
        # Select the closest match to the input statement
        closest_match = self.get(input_statement)
        # attitudelist = ()
        # print(closest_match)

        self.logger.info('Using "{}" as a close match to "{}"'.format(
            input_statement.text, closest_match.text
        ))

        # Get all statements that are in response to the closest match
        response_list = self.chatbot.storage.filter(
            in_response_to__contains=closest_match.text
        )
        if self.cmpid != '-1' and self.serviceid != '-1' and self.cid != '-1' and self.askid != '-1':

            keywords = self.selskill(self.serviceid)
            # print(keywords)
            if len(keywords) != 0:
                for i in keywords:
                    for keyword in i[1].split(','):
                        if keyword.strip() in str(input_statement):
                            closest_match = input_statement
                            skillid = i[0]
                            if skillid != "":
                                closest_match.text = 'skill' + str(skillid)
                                closest_match.confidence = 1
                                return closest_match
        if response_list:
            self.logger.info(
                'Selecting response from {} optimal responses.'.format(
                    len(response_list)
                )
            )

            response = self.select_response(input_statement, response_list)

            response.confidence = closest_match.confidence
            self.logger.info('Response selected. Using "{}"'.format(response.text))

            if self.cmpid != '-1' and self.serviceid != '-1' and self.cid != '-1' and self.askid != '-1' and response.confidence != 1:
                # attitudelist = self.ifhoodlemobole(self.askid)
                data = self.groo_keywork(input_statement, response)
                if data != None:
                    return data
                back = self.attitudejudge(closest_match, input_statement)
                # print('back',back)
                if back != None:
                    if back[0] != 1:
                        if back[0] != 0:
                            if float(response.confidence) < 0.85:
                                if float(back[0]) == float(response.confidence):
                                    if len(back[1]) > len(response.text):
                                        response.confidence = back[0]
                                        response.text = back[1]
                                elif float(back[0]) > float(response.confidence):
                                    response.confidence = back[0]
                                    response.text = back[1]
                    else:
                        response.confidence = back[0]
                        response.text = back[1]
            response.confidence = 1
            if self.cmpid != '-1' and self.serviceid != '-1' and self.cid != '-1' and self.askid != '-1':
                if response.text[0] == 'A':
                    self.insertquestion(response.text[1:], self.cid, self.cmpid)
        else:
            response = closest_match
            if self.cmpid != '-1' and self.serviceid != '-1' and self.cid != '-1' and self.askid != '-1':
                data = self.groo_keywork(input_statement, response)
                if data != None:
                    return data
                back = self.attitudejudge(closest_match, input_statement)
                attitudelist = self.ifhoodlemobole(self.askid)
                if back != None:
                    if float(back[0]) > float(self.confidence_threshold):
                        response.confidence = 1
                        response.text = back[1]
                        return response

                regword = self.pipe_reg_attitue(input_statement, response)
                if regword != None:
                    return regword

                if (5,) in attitudelist:
                    response = closest_match
                    response.confidence = 1
                    response.text = 'attitude5'
                    return response

                response = closest_match
                response.confidence = 1
                response.text = 'notnext'
                return response

            else:

                response.confidence = 0
                response.text = self.default_response

            self.logger.info(
                'No response to "{}" found. Selecting a random response.'.format(
                    closest_match.text
                )
            )

            # Set confidence to zero because a random response is selected

        return response
