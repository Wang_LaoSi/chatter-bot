from __future__ import unicode_literals

import warnings
from collections import Counter

from chatterbot import utils
from .logic_adapter import LogicAdapter


class MultiLogicAdapter(LogicAdapter):
    """
    MultiLogicAdapter allows ChatterBot to use multiple logic
    adapters. It has methods that allow ChatterBot to add an
    adapter, set the chat bot, and process an input statement
    to get a response.
    multilogicadapter允许聊天机器人使用多个逻辑
    适配器。它的方法，让聊天机器人添加
    适配器，设置聊天机器人，并处理输入语句。
    得到答复。
    """

    def __init__(self, **kwargs):
        super(MultiLogicAdapter, self).__init__(**kwargs)

        # Logic adapters added by the chat bot通过聊天机器人添加逻辑适配器
        self.adapters = []

        # Requied logic adapters that must always be present要求逻辑适配器必须存在
        self.system_adapters = []

    def get_initialization_functions(self):
        """
        Get the initialization functions for each logic adapter.
        每个逻辑适配器的初始化函数。
        """
        functions_dict = {}

        # Interate over each adapter and get its initialization functions
        # 需要在每个适配器和得到它的初始化函数
        for logic_adapter in self.get_adapters():
            functions = logic_adapter.get_initialization_functions()
            functions_dict.update(functions)

        return functions_dict

    def process(self, statement):
        """
        Returns the output of a selection of logic adapters
        for a given input statement.

        :param statement: The input statement to be processed.

        返回一个选择逻辑适配器输出
        对于一个给定的输入语句。
        声明：参数：输入的语句进行处理。
        """
        results = []
        result = None
        max_confidence = -1

        for adapter in self.get_adapters():
            if adapter.can_process(statement):

                output = adapter.process(statement)

                if type(output) == tuple:
                    warnings.warn(
                        '{} returned two values when just a Statement object was expected. '
                        'You should update your logic adapter to return just the Statement object. '
                        'Make sure that statement.confidence is being set.'.format(adapter.class_name),
                        DeprecationWarning
                    )
                    output = output[1]

                results.append((output.confidence, output,))

                self.logger.info(
                    '{} selected "{}" as a response with a confidence of {}'.format(
                        adapter.class_name, output.text, output.confidence
                    )
                )

                if output.confidence > max_confidence:
                    result = output
                    max_confidence = output.confidence
            else:
                self.logger.info(
                    'Not processing the statement using {}'.format(adapter.class_name)
                )

        # If multiple adapters agree on the same statement,
        # then that statement is more likely to be the correct response
        # 如果多个适配器同意相同的语句，然后，
        # 声明很可能是正确的反应
        if len(results) >= 3:
            statements = [s[1] for s in results]
            count = Counter(statements)
            most_common = count.most_common()
            if most_common[0][1] > 1:
                result = most_common[0][0]
                max_confidence = self.get_greatest_confidence(result, results)

        result.confidence = max_confidence
        return result

    def get_greatest_confidence(self, statement, options):
        """
        Returns the greatest confidence value for a statement that occurs
        multiple times in the set of options.

        :param statement: A statement object.
        :param options: A tuple in the format of (confidence, statement).

        返回一个表，发生最大的信心值
        在选项的设置多个时间。
        声明：声明：参数对象。
        ：参数选择：格式中的一个元组（自信、语句）。
        """
        values = []
        for option in options:
            if option[1] == statement:
                values.append(option[0])

        return max(values)

    def get_adapters(self):
        """
        Return a list of all logic adapters being used, including system logic adapters.
        返回一个列表中的所有逻辑适配器使用，包括系统逻辑适配器。
        """
        adapters = []
        adapters.extend(self.adapters)
        adapters.extend(self.system_adapters)
        return adapters

    def add_adapter(self, adapter, **kwargs):
        """
        Appends a logic adapter to the list of logic adapters being used.

        :param adapter: The logic adapter to be added.
        :type adapter: `LogicAdapter`
        附加逻辑适配器使用逻辑适配器列表。
        ：该适配器：可添加逻辑适配器。
        ：：` logicadapter `型适配器
        """
        utils.validate_adapter_class(adapter, LogicAdapter)
        adapter = utils.initialize_class(adapter, **kwargs)
        self.adapters.append(adapter)

    def insert_logic_adapter(self, logic_adapter, insert_index, **kwargs):
        """
        Adds a logic adapter at a specified index.

        :param logic_adapter: The string path to the logic adapter to add.
        :type logic_adapter: str

        :param insert_index: The index to insert the logic adapter into the list at.
        :type insert_index: int
        添加指定索引处的逻辑适配器。
        logic_adapter：参数：字符串路径的逻辑适配器添加。
        ：型logic_adapter：STR
        insert_index：参数：插入逻辑适配器插入到列表中的索引。
        ：insert_index：int类型
        """
        utils.validate_adapter_class(logic_adapter, LogicAdapter)

        NewAdapter = utils.import_module(logic_adapter)
        adapter = NewAdapter(**kwargs)

        self.adapters.insert(insert_index, adapter)

    def remove_logic_adapter(self, adapter_name):
        """
        Removes a logic adapter from the chat bot.

        :param adapter_name: The class name of the adapter to remove.
        :type adapter_name: str
        从聊天机器人中删除一个逻辑适配器。
        ：参数adapter_name：删除适配器类的名称。
        ：型adapter_name：STR
        """
        for index, adapter in enumerate(self.adapters):
            if adapter_name == type(adapter).__name__:
                del self.adapters[index]
                return True
        return False

    def set_chatbot(self, chatbot):
        """
        Set the chatbot for each of the contained logic adapters.

        对于每个包含的逻辑适配器设置聊天。
        """
        super(MultiLogicAdapter, self).set_chatbot(chatbot)

        for adapter in self.get_adapters():
            adapter.set_chatbot(chatbot)
