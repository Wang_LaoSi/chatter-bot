from __future__ import unicode_literals

from chatterbot.conversation import Statement
from chatterbot.input import InputAdapter
from chatterbot.utils import input_function


class TerminalAdapter(InputAdapter):
    """
    A simple adapter that allows ChatterBot to
    communicate through the terminal.

    一个简单的适配器，可以让聊天机器人来
    通过终端通信。
    """

    def process_input(self, *args, **kwargs):
        """
        Read the user's input from the terminal.

        从终端读取用户的输入。
        """
        user_input = input_function()
        return Statement(user_input)
