from __future__ import unicode_literals

# from chatterbot.adapters import Adapter
from ..adapters import Adapter


class InputAdapter(Adapter):
    """
    This is an abstract class that represents the
    interface that all input adapters should implement.
    这是一个抽象类表示
    所有的输入接口适配器应实施。
    """

    def process_input(self, *args, **kwargs):
        """
        Returns a statement object based on the input source.
        返回一个基于输入源语句对象。
        """
        raise self.AdapterMethodNotImplementedError()

    def process_input_statement(self, *args, **kwargs):
        """
        Return an existing statement object (if one exists).
        返回现有语句对象（如果存在）。
        """
        input_statement = self.process_input(*args, **kwargs)
        self.logger.info('Recieved input statement: {}'.format(input_statement.text))

        existing_statement = self.chatbot.storage.find(input_statement.text)

        if existing_statement:
            self.logger.info('"{}" is a known statement'.format(input_statement.text))
            input_statement = existing_statement
        else:
            self.logger.info('"{}" is not a known statement'.format(input_statement.text))

        return input_statement
