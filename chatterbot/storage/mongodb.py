from chatterbot.conversation import Response
from chatterbot.storage.storage_adapter import StorageAdapter


class Query(object):

    def __init__(self, query={}):
        self.query = query

    def value(self):
        return self.query.copy()

    def raw(self, data):
        query = self.query.copy()

        query.update(data)

        return Query(query)

    def statement_text_equals(self, statement_text):
        query = self.query.copy()

        query['text'] = statement_text

        return Query(query)

    def statement_text_not_in(self, statements):
        query = self.query.copy()

        if 'text' not in query:
            query['text'] = {}

        if '$nin' not in query['text']:
            query['text']['$nin'] = []

        query['text']['$nin'].extend(statements)

        return Query(query)

    def statement_response_list_contains(self, statement_text):
        query = self.query.copy()

        if 'in_response_to' not in query:
            query['in_response_to'] = {}

        if '$elemMatch' not in query['in_response_to']:
            query['in_response_to']['$elemMatch'] = {}

        query['in_response_to']['$elemMatch']['text'] = statement_text

        return Query(query)

    def statement_response_list_equals(self, response_list):
        query = self.query.copy()

        query['in_response_to'] = response_list
        return Query(query)


class MongoDatabaseAdapter(StorageAdapter):
    """
    The MongoDatabaseAdapter is an interface that allows
    ChatterBot to store statements in a MongoDB database.

    :keyword database: The name of the database you wish to connect to.
    :type database: str

    .. code-block:: python

       database='chatterbot-database'

    :keyword database_uri: The URI of a remote instance of MongoDB.
    :type database_uri: str

    .. code-block:: python

       database_uri='mongodb://example.com:8100/'
       这是一个接口，允许mongodatabaseadapter
       聊天机器人存储在MongoDB数据库报表。
       关键词：数据库：您要连接到的数据库的名称。
       类型：STR数据库：
       ..：：Python代码块
       数据库= 'chatterbot-database”
       关键词：：database_uri的MongoDB远程实例的URI。
       ：型database_uri：STR
       ..：：Python代码块
       database_uri = 'mongodb：/ /例子。com：8100 /”
    """

    def __init__(self, **kwargs):
        super(MongoDatabaseAdapter, self).__init__(**kwargs)

        self.database_name = self.kwargs.get(
            'database', 'chatterbot-database'
        )
        self.database_uri = self.kwargs.get(
            'database_uri', 'mongodb://localhost:27017/'
        )

        # Use the default host and port
        # self.client = MongoClient(self.database_uri, serverSelectionTimeoutMS=3,auto_start_request=False)
        self.client = self.database_uri
        # print('self.database_uri',self.database_uri)
        # self.originaldatabase = self.database_name

        # Specify the name of the database

        # MongoClient.maxAutoConnectRetryTime = True
        # The mongo collection of statement documents
        self.newdatabasename = self.kwargs.get(
            'newdatabasename', 'statements'
        )
        self.database = self.client[self.database_name]
        self.statements = self.database[self.newdatabasename]
        # if self.database_name.count('-')>0:
        #
        #     data = self.database_name.split('-')
        #     self.database = self.client[data[0]]
        #     self.statements = self.database[self.database_name]
        # else:
        #
        #     self.database = self.client[self.database_name]
        #
        #     self.statements = self.database[self.newdatabasename]

        # self.statements = self.database['statements']

        # Set a requirement for the text attribute to be unique
        self.statements.create_index('text', unique=True)

        self.base_query = Query()

    def count(self):
        return self.statements.count()

    def find(self, statement_text):
        # {}
        query = self.base_query.statement_text_equals(statement_text)
        # {'text':statement_text}
        values = self.statements.find_one(query.value())
        # {'text':statement_text}
        if not values:
            return None

        del values['text']

        # Build the objects for the response list为响应列表构建对象
        values['in_response_to'] = self.deserialize_responses(
            values.get('in_response_to', [])
        )

        return self.Statement(statement_text, **values)

    def deserialize_responses(self, response_list):
        """
        Takes the list of response items and returns
        the list converted to Response objects.
        以响应项目并返回列表
        列表转换为响应对象。
        """
        proxy_statement = self.Statement('')
        for response in response_list:
            text = response['text']
            del response['text']

            proxy_statement.add_response(
                Response(text, **response)
            )
        return proxy_statement.in_response_to

    def mongo_to_object(self, statement_data):
        """
        Return Statement object when given data
        returned from Mongo DB.
        给定数据时返回语句对象
        从Mongo DB返回。
        """
        statement_text = statement_data['text']
        del statement_data['text']

        statement_data['in_response_to'] = self.deserialize_responses(
            statement_data.get('in_response_to', [])
        )

        return self.Statement(statement_text, **statement_data)

    def filter(self, **kwargs):
        """
        Returns a list of statements in the database
        that match the parameters specified.
        返回数据库中的报表
        匹配指定的参数。
        """
        import pymongo
        query = self.base_query

        order_by = kwargs.pop('order_by', None)

        # Convert Response objects to data将响应对象数据
        if 'in_response_to' in kwargs:
            serialized_responses = []
            for response in kwargs['in_response_to']:
                serialized_responses.append({'text': response})

            query = query.statement_response_list_equals(serialized_responses)

            del kwargs['in_response_to']

        if 'in_response_to__contains' in kwargs:
            query = query.statement_response_list_contains(
                kwargs['in_response_to__contains']
            )
            del kwargs['in_response_to__contains']

        query = query.raw(kwargs)

        matches = self.statements.find(query.value())

        if order_by:

            direction = pymongo.ASCENDING

            # Sort so that newer datetimes appear first更新日期排序，先出现
            if order_by == 'created_at':
                direction = pymongo.DESCENDING

            matches = matches.sort(order_by, direction)

        results = []

        for match in list(matches):
            results.append(self.mongo_to_object(match))

        return results

    def update(self, statement):
        from pymongo import UpdateOne
        from pymongo.errors import BulkWriteError

        data = statement.serialize()
        operations = []

        update_operation = UpdateOne(
            {'text': statement.text},
            {'$set': data},
            upsert=True
        )
        operations.append(update_operation)

        # Make sure that an entry for each response is saved确保保存每个反应项
        for response_dict in data.get('in_response_to', []):
            response_text = response_dict.get('text')

            # $setOnInsert does nothing if the document is not createdsetoninsert不如果不创建文件
            update_operation = UpdateOne(
                {'text': response_text},
                {'$set': response_dict},
                upsert=True
            )
            operations.append(update_operation)

        try:
            self.statements.bulk_write(operations, ordered=False)
        except BulkWriteError as bwe:
            # Log the details of a bulk write error
            self.logger.error(str(bwe.details))

        return statement

    def get_random(self):
        """
        Returns a random statement from the database
        返回从数据库中随机的声明
        """
        from random import randint

        count = self.count()

        if count < 1:
            raise self.EmptyDatabaseException()

        random_integer = randint(0, count - 1)

        statements = self.statements.find().limit(1).skip(random_integer)

        return self.mongo_to_object(list(statements)[0])

    def remove(self, statement_text):
        """
        Removes the statement that matches the input text.
        Removes any responses from statements if the response text matches the
        input text.
        删除匹配的输入文本声明。
        如果响应文本匹配，则从语句中删除任何响应。
        输入文本。
        """
        for statement in self.filter(in_response_to__contains=statement_text):
            statement.remove_response(statement_text)
            self.update(statement)

        self.statements.delete_one({'text': statement_text})

    def get_response_statements(self):
        """
        Return only statements that are in response to another statement.
        A statement must exist which lists the closest matching statement in the
        in_response_to field. Otherwise, the logic adapter may find a closest
        matching statement that does not have a known response.
        只返回对另一语句的响应语句。
        语句必须存在，它列出最接近的匹配语句
        in_response_to场。否则，逻辑适配器可能会找到一个最近的
        没有已知响应的匹配语句。
        """
        # print(self.statements,self.database_name)
        # if self.database_name.count('-')==1:
        #
        #     data = self.database_name.split('-')
        #     self.database = self.client[data[0]]
        #     #print(self.database)
        #     self.statements = self.database[self.database_name]
        # print(self.statements)

        # response_query = self.statements.distinct('in_response_to.text')
        response_query = self.statements.distinct('in_response_to.text')

        _statement_query = {
            'text': {
                '$in': response_query
            }
        }
        _statement_query.update(self.base_query.value())

        # statement_query = self.statements.find(_statement_query)
        statement_query = self.statements.find(_statement_query)

        statement_objects = []

        for statement in list(statement_query):
            statement_objects.append(self.mongo_to_object(statement))
        # print('statement_objects',statement_objects,'dd')
        return statement_objects

    def get_responseandanswer_statements(self, diccondition):
        """

        """
        response_query = list(self.statements.find({"text": {"$in": diccondition}}).distinct('text'))

        _statement_query = {
            'text': {
                '$in': response_query
            }
        }
        _statement_query.update(self.base_query.value())

        statement_query = self.statements.find(_statement_query)

        statement_objects = []

        for statement in list(statement_query):
            statement_objects.append(self.mongo_to_object(statement))

        return statement_objects

    def backdatatable(self, database_name):
        # self.database_name = self.originaldatabase
        if database_name.count('-') == 1:
            data = self.database_name.split('-')
            self.database = self.client[data[0]]
            # print(self.database)
            self.statements = self.database[self.database_name]
            # print(self.statements)

        else:
            self.database = self.client[database_name]
            self.statements = self.database['statements']

    # 查询是否存在表
    def seltable(self, childtablename, tablename):
        try:
            # dblist = self.client.database_names()
            # print(dblist)
            # if str(tablename) in dblist:
            #    print(tablename)
            # print(childtablename,tablename)
            if tablename != "":
                db = self.client[tablename]
                return db[childtablename].count()
            # if tablename.count('-')==1:
            #     data = tablename.split('-')
            #     databasename = data[0]
            #     db = self.client[databasename]
            #     return db[tablename].count()
            else:

                db = self.client[tablename]
                return db.statements.count()

            # else:
            #     return '0'
        except:
            # if client.connect!=True:
            #      return seltable(tablename)
            pass
            # pass
        finally:
            client = None

    def get_response_statements_database(self, database, tablename):
        """
        Return only statements that are in response to another statement.
        A statement must exist which lists the closest matching statement in the
        in_response_to field. Otherwise, the logic adapter may find a closest
        matching statement that does not have a known response.
        只返回对另一语句的响应语句。
        语句必须存在，它列出最接近的匹配语句
        in_response_to场。否则，逻辑适配器可能会找到一个最近的
        没有已知响应的匹配语句。
        """
        if tablename != "":
            self.database = self.client[tablename]
            self.statements = self.database[database]

        else:
            self.database = self.client[database]
            self.statements = self.database['statements']

        # originaldatabase = self.database_name
        # self.database_name = database
        #
        # #print(self.statements,self.database_name)
        # if self.database_name.count('-')==1:
        #
        #     data = self.database_name.split('-')
        #     self.database = self.client[data[0]]
        #     #print(self.database)
        #     self.statements = self.database[self.database_name]
        #     #print(self.statements)
        # else:
        #     self.database = self.client[self.database_name]
        #     self.statements = self.database['statements']
        #

        # response_query = self.statements.distinct('in_response_to.text')
        response_query = self.statements.distinct('in_response_to.text')

        _statement_query = {
            'text': {
                '$in': response_query
            }
        }
        _statement_query.update(self.base_query.value())

        # statement_query = self.statements.find(_statement_query)
        statement_query = self.statements.find(_statement_query)

        statement_objects = []

        for statement in list(statement_query):
            statement_objects.append(self.mongo_to_object(statement))
        # print('statement_objects',statement_objects,'dd')
        return statement_objects

        # self.database = self.client[self.database_name]
        # self.statements = self.database['statements']
        #
        # response_query = self.statements.distinct('in_response_to.text')
        #
        #
        #
        # _statement_query = {
        #     'text': {
        #         '$in': response_query
        #     }
        # }
        #
        # _statement_query.update(self.base_query.value())
        #
        # statement_query = self.statements.find(_statement_query)
        #
        #
        # statement_objects = []
        #
        # for statement in list(statement_query):
        #
        #     statement_objects.append(self.mongo_to_object(statement))
        #
        #
        # # self.database_name = originaldatabase
        # # self.database = self.client[originaldatabase[1]]
        # # self.statements = self.database['statements']
        #
        # return statement_objects

    def drop(self):
        """
        Remove the database.
        删除数据库。
        """
        self.client.drop_database(self.database_name)


'''    def __delattr__(self):
        self.client.close()
        self.client=None


    def __del__(self):
        self.client.disconnect()
        self.client=None
'''
