from chatterbot.zyrobot import unknowquestion
from logging.config import dictConfig

from chatterbot.zyrobot.logger_config import FORMATTERS, FILTERS, HANDLERS, LOGGERS
dictConfig({
    'version': 1,
    'formatters': FORMATTERS,
    'filters': FILTERS,
    'handlers': HANDLERS,
    'loggers': LOGGERS,
})

app = unknowquestion.app

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=8083, debug=False, threaded=True)
